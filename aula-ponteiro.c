
#include <stdlib.h>
#include <stdio.h>
#define pi 3.1416

void area_volume(float r, float *a, float *v){

	*a = pi * r * r;
	*v = (4/3) * (pi*r*r*r);
}

int main() {
	float area, volume, raio;

	printf("Informe o raio\n");
	scanf("%f", &raio);
	area_volume(raio, &area, &volume);
	printf("area é %.2f\n", area);
	printf("o volume e %.2f\n", volume);



	return EXIT_SUCCESS;
}
